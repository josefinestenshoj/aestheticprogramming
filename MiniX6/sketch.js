//My global variables
let earth;
let earthImg; //earth image
let plasticImg; //plastic bag image
let backgroundImg //background image
let plasticArray = [];


function preload() {
  earthImg = loadImage('earth.png');
  plasticImg = loadImage('plastic.png');
  backgroundImg = loadImage('background.jpg');
  myFont = loadFont('earthbound-beginnings.ttf')
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  earth = new Earth();
}

function keyPressed() {
  if (key == ' ') {
    earth.jump(); //calling the jum function from earth.js
  }
}


function draw() {
  if (random(1) < 0.01) { //when to push a new plastic bag
    plasticArray.push(new Plastic());

  }

  background(backgroundImg);
  earth.move();
  earth.show();
  //

  //Instructions for the user
  fill(15,56,125);
  textSize(15);
  textFont(myFont);
  text("PRESS SPACE TO AVOID THE PLASTIC!", width - 900, 100);
  //Slogan to make the user think about his/her actions.
  fill(255,180);
  textSize(20)
  text("SAVE THE EARTH", width - 800, 130);

  for (let p of plasticArray) {
    p.show();
    p.move();


    if (earth.hits(p)) {
      noLoop(); //to stop the game
      //Text appear if the user loose
      textSize(50);
      textAlign(CENTER);
      textStyle(BOLD);
      fill(161,12,12);
      text("GAME OVER", width / 2, height / 2);
      textSize(20);
      text("please minimize the use of plastic and be consious..", width / 2, height / 2 + 40);
}
}
}
