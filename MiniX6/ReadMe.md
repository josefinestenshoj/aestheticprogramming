## MiniX: OBJECT ABSTRACTION

[Link to my RunMe](https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX6/)


<img src="billede1.png" width="800">
<img src="billede2.png" width="800">



***MiniX6 OBJECT ORIENTED PROGRAMMING***

**Describe how does/do your game/game objects work?**

In my sixth MiniX i made a game that potrays climate issues (specifically plastic use and it's effect on the earth/climate). The game is inspired by the code of the game "Flappy Bird". When you press the spacebar you get to move the earth. Plastic will appear from the right side going to the left and the challenge is to avoid the earth hitting the plastic bags. If you hit one of the plastic bags you loose the game and a text will appear saying: "Game Over" and a little message that is supposed to make the user think about subsequently. 

**Describe how you program the objects and their related attributes, and the methods in your game.**

My game consist of the two classes earth.js and plastic.js (Which represents the earth and the plastic bags)

In the beginning I made a class for the earth - I made a class to the object by giving it specific instructions. In the earth.js class I used five different methods: 

Construction: Gives the earth specific variables (size, position, speed etc.)
Jump: Makes it possible for the user to move the earth jump up and down.
Hits: Detects when the earth hits a plastic bag
Move: In relation to the jump behavior, but a constraint is set here to control the gravity of the earth so it stays in the canvas.
Show: Makes the png. file appear on the canvas.

Similarily with the plastic bags:

Construction: Gives the plastic bags specific variables (size, position, speed etc.)
Move — Decides how fast the plastic bags move and ind which direction. 
Show — makes the png. file appear on the canvas.

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction? Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?**


Growing up I played a lot of different games - but thinking back none of the games dealt with any "serious" issues or matters. I'm not up to date on which games are available today, but I think the "game industry" has a big impact on the world and a great platform to be informative and ilumminative to important topics. Just as Fuller and Goffey mentiones in the text: 

Fuller and Goffey suggest that this Object-Oriented Modelling of the world is a social-technical practice, "compressiong and abstracting relations operative at different scales of realities, composing new forms of agency."

We have agency to make a change - not to say the game that i created does that, but I wanted it to touch upon an important topic and give the user something to think about. A lot of people are motivated by competition and on that note I think gaming has a great impact by motivating the user. The user doesn't want to loose so they have to avoid hitting the plastic.

I also see games as a way to escape reality and transform into a new world (just like we do reading a book, watching TV-shows or listening to music/podcast). I also thinks this connect well with Object abstraction in relation to games. This could be a reason why there are so many gamers around the world. I don't consider myself a gamer, but i do see it as a whole community. Contra reading a book or watching a TV-show, you are more involved when playing a game. Your actions matter and can lead to benefits or consequences. We are often able to (in some games) customize our own avatar (an object that can represent us). This gives us a chance to escape who we are seen or labeled as in real life and gives us the opportunity to choose who we want to become or represent to others in the game. Many of us read books or watch TV-shows to escape from the person and world we live in and "pretend" to be someone else and see the world through different eyes. You are able to do exactly this when gaming while interacting with others and being able to control your object even more. 

## References

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on BrightSpace\Literature)

Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

p5.js examples - Objects,” https://p5js.org/examples/objects-objects.html.


