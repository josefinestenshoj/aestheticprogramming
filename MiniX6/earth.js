class Earth {
  constructor() {
    this.r = 100; //Size of the earth
    this.x = 500; //Position on the x-axis
    this.y = height - this.r; //position on the y-axis
    this.vy = 0; //velocity of speed along the y-axis
    this.gravity = 2; //adjusting the speed of the earth
  }
  //If jump function is activated the earth moves up the y-axis
  jump() {
    this.vy = -35;
  }

  hits(plastic) {
    return collideRectRect(this.x, this.y, this.r, this.r, plastic.x, plastic.y, plastic.r, plastic.r);
  }
//Determines if the earth and the plastic bags collides using their hight and width

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
  
    this.y = constrain(this.y, 0, height - this.r);
       //The earth is only allowed to move up to a certain point
  }

  //To make the earth appear on the canvas
  show() {
    image(earthImg, this.x, this.y, this.r, this.r);
  }
}

