## MiniX4 submission


### MINIX4 - DATA GATHERING

<img src="MiniX4.png" width="700">

RunMe: https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX4/

My code 

***Provide a title for and a short description of your work as if you were going to submit it to the festival.***

# BEHIND THE COOKIES
"Behind the cookies" ia an artwork exposing the part of cookies most of us never think of. It adresses the seriousness of giving access to out data  to different softwares, and what 'accepting a cookie' might really mean. Of course this artwork isn't based on the what specific data is gathered when giving consent to a given website, but more to show an exaggerated result of which data could be captured from the user. 

Describe your program and what you have used and learnt.
In my program I've worked with buttons, arrays, mousePressed and while loop. When seeing the program at first you will meet a button that says: "Please accept our cookies". When clicking this button a ton of different 'data' will appear on the left side of the screen moving down. I have created a canvas that is windowWidth and windowHeight in a green background. I set the framerate to 60 frames per second to make sure the text runs pretty smoothly. I created the button and worked with boolean: (button.mousePressed(() => {buttonPressed = true});). I wanted to challenge myself with changing my buttons apperance and therefore using CSS styling. I styled the button to hover and create a drop-down shadow when the mouse is within the area of the button. I also added a color and font for the button. I made all of these classes in a styles.css document and added them by using addClass(''). 
To make the text move I...

# Articulate how your program and thinking address the theme of “capture all.”

My program addresses the theme “Capture all” by forcing the viewer to think what data is captured when accepting cookie or agreeing to terms and conditions to a specific website. I wanted to highlight how much a website actually know about you personally when accepting cookies and therefore why you should be mindful of what you are agreeing too, when using the internet. It might be a bit exaggerated in my program, because I don’t know what data specifically gets captured when agreeing to different terms, but it is more supposed to awaken and enlighten you to think once more before agreeing to different terms and conditions.


# What are the cultural implications of data capture?

In my generation (the digital age), I don’t think many of us are aware of how much value our personal data has to different websites.  We  agree to the terms, to access what we want on the internet, without further thinking of what we have just agreed upon. I find it intimidating: 

Our data, “the patterns of human behaviors,” is extracted and circulated within the logic of what Shoshana Zuboff calls “surveillance capitalism,” demonstrating the need for large quantities of all manner of data to be harvested for computational purposes, such as predictive analytic - Soon & Cox 2020

When I realized that my data were capitalized upon and used to predict future patterns and lure me into buying things, based on my data history. It’s also scary (at least for me) to think about my data as someone else’s property - It really put things into perspective for me, and I am now more mindful when browsing the internet. I do think most of my generation will just keep using the internet and different social media as always, because we are so used to just accepting cookies and different terms and conditions without ever facing the consequences/dark sides of "selling" our data. Therefore I think it is crucial to make artwork of some sort or change the way we deliver the "accept cookies/terms" to users, to make the think more critically and bring awareness about the down sides. 

## References

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

“p5.js examples - Interactivity 1,” https://p5js.org/examples/hello-p5-interactivity-1.html.

“p5 DOM reference,” https://p5js.org/reference/#group-DOM.

Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021. https://policyreview.info/concepts/datafication
