let cookieArray = ['Cheked in: Aarhus region', '03.57 pm: liked cafe Englen', 'texted mom: going through a divorce?', '20.12.2019: golden retriver recognized in photo', 'googled: signs of depression', '02.24 pm sent: aunts birthday tomorrow!','08.48 am: deleted picture',"Spotify 9-10pm: listened to Frank Sinatra" , " 04.04 am: swipes through tinder", "2 hours ago: parked at work", "07.18 pm: used VISA creditcard XXXX XXXX XXXX 3476", "01.13 pm: Searched for møbler", "13.02.2021 sent: looking for a new apartment? ", "googled: possible fracture?", "watched: what i eat in a day", "spend 5 hours and 38 minutes on iPhone", "googled: how do you know if you're an alcoholic?", "googled: is it normal to bleed for 10 days?", "googled: is it dangerous if your dog eats an entire box of chocolate?", "Deep sleep from 10.47pm to 03.35 am", "12.03.2022: reached 12.307 steps "];
//Creating array "cookieArray", and adding strings

let drawIteration = 0;
let showCookies = false;
//Initialize the cookies, so it doesn't show before it's pressed
//declaring global varaibles

let squaretf;
function preload(){
squaretf = loadFont('Square.ttf');
//loading font using preload function
}
function setup() {
  createCanvas(windowWidth,windowHeight);
  background(247,247,232);
  frameRate(60);
  //Specifies the number of frames to be displayed every second

  button=createButton('PLEASE ACCEPT OUR COOKIES 🥺');
  button.position(800,500,200,200);
  button.mousePressed(acceptCookies);
  //mousePressed() function is called once after every time a mouse button is pressed over the element.
  button.addClass('cookieButton');
  button.addClass('cookieButton:hover')
  //Adds specified class to the element.
  
}

function acceptCookies() {
showCookies = true;
// Initialize showCookies to be true
}

function cookies(){

  background(247,247,232);
  fill(131,110,6);
  textSize(18);
  textFont(squaretf);

  let index = 0;
  let y = 0;
  while (index < cookieArray.length) {
    text(cookieArray[index],50,(y+drawIteration) % windowHeight);
    index++;
    y+=30;
    //50 is my x-coordinate
    //Add drawIteration so that it becomes a scrolling text
    //Modulo% finds the rest and makes the text keep appearing on the screen, so that the y+drawIteraion doesn't become too large
  }
}


function draw() {
  if(showCookies) {
    cookies();
    //If the button was pressed in setup and showCookies are true the function "cookies()" will run
  }
  drawIteration++;
  //
}
