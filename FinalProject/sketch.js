//Declaring global variables
let canvas;
let textbox;
let describe;
let firstheader;
let secondheader;
let fontheader;
let greeting;
let describefont;

//Buttons
let submitButton
let yesButton;
let noButton;
let calculatedButton;
let startoverButton;
let redButton;

//IMAGES
let warning;
let warning2;

//CALCULATIONS
let digitalGreenprintCounter = 0;
let digitalGreenprintCounterText;
let digitalGreenprintClick = 0.08;
let digitalGreenprintClickText;

//RANKING LIST
let best;
let greenprintIndex;


//Preloading fonts, JSON and images
function preload(){
  fontheader = loadFont('BebasKai.ttf');
  namefont = loadFont('Basic_Light.ttf');
  describefont = loadFont('Roboto-Light.ttf');
  numbers = loadJSON('numbers.json');
  highRank= loadJSON("highestScore.json");
  middleRank = loadJSON("middleScore.json");
  lowRank = loadJSON("lowestScore.json");
  whyamiseeingthis = loadImage("datacapture.png");
  warningimg = loadImage("Warning.png");
  warning2img = loadImage("Warning2.png");
}

function setup() {
  canvas = createCanvas(windowWidth,windowHeight);
  noStroke();
  frameRate(40);

  setupsketch(); //callback
}

//Creating the energy consumption calculator with time spend on the server
function draw() {
  digitalGreenprintCounter+= 1/60
if(digitalGreenprintCounterText) {
  //does this text exist - does this variable have a value
  digitalGreenprintCounterText.html(digitalGreenprintCounter.toPrecision(3));
//This number is in my html - If an argument is given, sets the inner HTML of the element, replacing any existing html. 
  }
}

//Creating the energy consumption calculators with click on the canvas
function mousePressed () {
  digitalGreenprintClick+=0.08;
  if(digitalGreenprintClickText){
    digitalGreenprintClickText.html(digitalGreenprintClick.toPrecision(3));
  }
}

//The frontpage
function setupsketch() {

  background(247,242,242);
  //making the headline
    textAlign(CENTER);
    textSize(45);
    textFont(fontheader);
    firstheader = text('Type your full name to see your digital greenprint',windowWidth/2,windowHeight/2);

    push();
    textSize(25);
    textFont(describefont);
    describe = text('Do you know how much energy you are wasting online?\n My digital greenprint represents your digital carbon foodprint that’s calculated with data about you and your online behavior. \n The higher your digital greenprint score, the greener you are.',windowWidth/2,windowHeight/4);
    pop();

    //making the thing you type in
    textbox = createInput();
    textbox.position(windowWidth/2-96,windowHeight/2+40);

    //making a bottom, where you can submit your name
    submitButton = createButton('SUBMIT');
    submitButton.position(windowWidth/2-54,windowHeight/2+80);
    submitButton.size(100,40);
    submitButton.addClass('Button1');
    submitButton.addClass('Button1:hover');
    submitButton.mousePressed(popup1);

    //creating the image taken when clicking 'yes'
    camcorder = createCapture(VIDEO);
    camcorder.size(800,800);
    camcorder.hide();

}

//reset function when "start over button" is pressed
function reset() {
  setupsketch();
  startoverButton.remove();

}

//Pop up window aflter clicking 'submit'
function popup1(){

  //inserting popup image with warning
  warning = image(warningimg,windowWidth/2-350,windowHeight/2-300,750,600);

  //creating the yes button which enables interface3
  yesButton = createButton('YES');
  yesButton.addClass('Button2:hover');
  yesButton.addClass('Button2');
  yesButton.position(windowWidth/2+100,windowHeight/2+50);
  yesButton.size(70,40);

  //removing the textbox and submitbutton so it doesn't interfere with the warning image
  textbox.remove();
  submitButton.remove();

  //By clicking YES button interface3 will appear
  yesButton.mousePressed(interface3);

  //By clicking the NO button interface 2 will appear
  noButton = createButton('NO')
  noButton.addClass('Button3:hover');
  noButton.addClass('Button3');
  noButton.position(windowWidth/2+180,windowHeight/2+50);
  noButton.size(70,40);
  noButton.mousePressed(interface2);

}

//Second warning with the opportunity to start over
function interface2() {

  warning2 = image(warning2img,windowWidth/2-400,windowHeight/2-350,870,700);

  //Creating the start over button
  startoverButton = createButton('START OVER')
  startoverButton.addClass('Button2:hover');
  startoverButton.addClass('Button2');
  startoverButton.position(windowWidth/2+180,windowHeight/2+20);
  startoverButton.size(100,50);

  //removing the buttons from previous interface
  yesButton.remove();
  noButton.remove();

  //by clicking the start over button you will return to the first interface
  startoverButton.mousePressed(reset);

}

function interface3() { //digital greenprint interface

  //styling the website
  background(247,242,242);
  stroke(216,216,216);
  strokeWeight(3);
  line(0, 60, width, 60);
  line(width/2-10, 60, width/2-10, height);

  //removing buttons from previous interfaces
  submitButton.remove();
  yesButton.remove();
  noButton.remove();
  textbox.remove();

  //loading the image which is captured when the user clicks "yes"
  image(camcorder,windowWidth/2-550,windowHeight/2-200,345,260);


  //Header "My Digital Greenprint"
  stroke(10);
  strokeWeight(1);
  textAlign(CENTER);
  textSize(45);
  fill(32,63,16);
  text('MY DIGITAL GREENPRINT',windowWidth/2,windowHeight/2-350);

  //Ellipse with greenprint percentage inside
  ellipse(windowWidth/2+280,windowHeight/2-110,300,300);
  fill(32,63,16);

  //Generating a random number within the array "Greenprint" in the JSON file
  greenprintIndex = int(random(0,numbers.Greenprint.length))
  //integer (casting it to an integer)
  //a random number between 0 and the length of the Greenprint array
  fill(255);
  textSize(150);
  text(numbers.Greenprint[greenprintIndex],windowWidth/2+150,windowHeight/2-200,300,300);

  //The user's name typed from the beginning is shown on the left side
  let name = textbox.value();
  fill(32,63,16);
  textSize(40);
  textFont(namefont);
  text(name,windowWidth/2-420,windowHeight/2-250);

  textSize(19);
  text('WHAT AMOUNT OF ENERGY YOU USE JUST BY \n RUNNING AND INTERACTING WITH THIS SERVER',windowWidth/2-400,windowHeight/2+130)


  // Counter of Mb used when using the server
  digitalGreenprintCounterText = createDiv(digitalGreenprintCounter);
  digitalGreenprintCounterText.style('font-size','45px');
  digitalGreenprintCounterText.style('text-align','center');
  digitalGreenprintCounterText.style('font-family', 'monospace');
  digitalGreenprintCounterText.style('color','#B71717');
  digitalGreenprintCounterText.style('font-weight','bold');
  digitalGreenprintCounterText.position(windowWidth/2-500,windowHeight/2+220);

  //Click that measures 0.08 grams of CO2
  digitalGreenprintClickText = createDiv(digitalGreenprintClick);
  digitalGreenprintClickText.style('font-size','45px');
  digitalGreenprintClickText.style('text-align','center');
  digitalGreenprintClickText.style('font-family', 'monospace');
  digitalGreenprintClickText.style('color','#B71717');
  digitalGreenprintClickText.style('font-weight','bold');
  digitalGreenprintClickText.position(windowWidth/2-500,windowHeight/2+280);

  //Creating Div's with unit's for the click and time spend on the server
  mb = createDiv('Mb')
  mb.style('font-size','30px');
  mb.style('text-align','center');
  mb.style('font-family', 'monospace');
  mb.style('color','#B71717');
  mb.position(windowWidth/2-352,windowHeight/2+232);

  gramsOfCo2 = createDiv('grams of CO2')
  gramsOfCo2.style('font-size','30px');
  gramsOfCo2.style('text-align','center');
  gramsOfCo2.style('font-family', 'monospace');
  gramsOfCo2.style('color','#B71717');
  gramsOfCo2.position(windowWidth/2-350,windowHeight/2+290);

  //Creating and styling button with calculations
  push();
  calculatedButton = createButton('HOW IS THIS CALCULATED?');
  calculatedButton.position(windowWidth/2+180,windowHeight/2+200);
  calculatedButton.size(200,100);
  calculatedButton.addClass('Button1');
  calculatedButton.addClass('Button1:hover');
  calculatedButton.mousePressed(interface4);
  pop();
}

  //the popup window with the reasons for the greenprint number
  function interface4() {

    let rankArray = [];
    let y = windowHeight/2-290;
    calculatedButton.remove();
    textAlign(LEFT);
    image(whyamiseeingthis,windowWidth/2-350,windowHeight/2-470,1100,800);

  if (greenprintIndex >= 0 && greenprintIndex <= 5){
  rankArray = lowRank.Best
  }
  if (greenprintIndex >= 6 && greenprintIndex <= 13){
    rankArray = middleRank.Middle
  }
  if (greenprintIndex >= 14 && greenprintIndex <= 20){
    rankArray = highRank.worst
  }
for(i = 0; i < rankArray.length; i++){
  textSize(15);
  text(rankArray[i],windowWidth/2-180,y);
  y = y + 33

  }
}
