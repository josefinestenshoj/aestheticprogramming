## MiniX: A generative program

[Link to my RunMe](https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX5/)

🤍Enable full screen for the best result🤍
You can change the way the program generates by making the browser bigger og smaller
<img src="screen1.png" width="600">
<img src="screen2.png" width="600">
<img src="screen3.png" width="600">


***MiniX5 - Auto-Generator***

**What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?**
For this week with the theme 'auto-generator' I decided to make an abstract program using minimalistic shapes and colors I liked. In my program I have made up a few simple rules to create a foundation for the auto generated program:

## 
1. Create ellipses moving from the top corner
2. When the ellipses hits the corner or ending of the window/canvas it shall push back in a 90 degree angle
3. When the ellipses hits the x axes' left or right - they will turn orange
4. When the ellipses hits the y axes' top or buttom they will turn light green

These rules generates an interesting pattern in the program. Not only does the ellipses create a form of line, but when they pass through each other they create different shapes such as rectangles and half circles. Depending on how big the window of your browser is, the program will run differently and thereby generate different forms and shapes. 


**What role do rules and processes have in your work?**
I think it's interesting that I only had a tiny idea or imagination on how the program would turn out by following these simple rules. After creating the program according to the rules I invented, I played around with different colors of the ellipses and with stroke/noStroke. I initally wanted the colors to appear randomly after the ellipse hit a corner or any end of the canvas - But after playing around with different colors I thought these specific colors created the most interesting pattern. I also found that the very thin strokes (strokeWeight(0.1)) creates an elegant and beautiful pattern. I also changed the parameter of the size of the ellipse. The big circle also created a much more 'unpredictable' pattern which I really liked. 

**Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?**

“Generative art refers to any art practice where artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.”
(Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 126)

I think this quote/definition of generative art is super interesting and awakening for me. Earlier on i never thought about how simple rules could result in complex and abstract art. I think the idea of us (as the artist) to lose control and not know how the artwork is gonna turn out is a super interesting approach. Similarly to classical art, Generative Art is a platform from which to create abstract art.
The creation of auto-generators opens up new creative perspectives because getting to the result is such a different proces from what we normally do. Normally we would maybe have an image of what we want to create and then we'll map our proces thereby. This also relates to how you code - do you start off by maybe creating a sketch or having a clear image of what you want to code or do you simply try different syntaxes and functions and slowly creating something unexpected. I think both of these approaches can create different beautiful art, but there is something interesting about letting go of the control we always have and being playful with the system (in this case our program) and making art with the system. There is something intreging about the fact that the artist might not only be the person sitting behind the screen, but also the program that is able to execute it. It is a difficult discussion, because I normally wouldn't give credit to the factory that making the specific paint or brushes that an artist uses to paint with. It is interesting to raise a question of who owns what property of an artwork - I personally think it's a merge of every piece of the process that have some sort of property or at least given a bit of credit. But in the end it is the artist who executes it and who initially started the proces. It kind of relates to the saying that "It isn't about what camera you use, it's about the photographer".


## References


[Daniel Shiffman, “p5.js - 2D Arrays in Javascript,” Youtube](https://www.youtube.com/watch?v=OTNpiLUSiB4)

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142


