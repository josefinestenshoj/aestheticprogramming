let dot;


function setup(){
createCanvas(windowWidth,windowHeight);
background(20);
frameRate(200);
dot = new Dot(0,0);
//Instantiate a new object of my Dot class
}

function draw(){
if (dot.x > windowWidth) {
  //If my dot hits the right side of the screen 
 dot.xdirection = dot.xdirection *(-1);
 //reverse x-direction if its further than windowWidth
 dot.color = color(165,85,16);
}
if (dot.y > windowHeight) {
  //If the dot hits the buttom of the screen
 dot.ydirection = dot.ydirection *(-1);
 dot.color = color(196,210,163);

}
if (dot.x < 0) {
  //If the dot hits the left side of the screen
 dot.xdirection = dot.xdirection *(-1);
 dot.color = color(165,85,16);

}
if (dot.y < 0) {
   //If the dot hits the top of the screen
 dot.ydirection = dot.ydirection *(-1);
 dot.color = color(196,210,163);

}
  dot.move();
  dot.display();

}

function Dot(x,y){
  this.x = x;
  this.y = y;
  this.xdirection = 3;
  this.ydirection = 3;
  //For every move(draw) it moves 3 pixels in the x and y direction
  this.color = color(165,85,16);



  this.display = function() {
    noFill();
    stroke(this.color);
    strokeWeight(0.1);
    ellipse(this.x,this.y,600);

  }

  this.move = function() {
    this.x = this.x + this.xdirection;
    this.y = this.y + this.ydirection;

}
}
