## MiniX submission


See my code [HERE] (https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX1/)
### See my MiniX in the file MiniX.mov!

<img src="Minix1billede.png" width="600">


***MiniX1***
In my first MiniX I decided to make an old car. I was interested in the use of ellipses and arches and how that could result in a small ‘bubble’ car. I wanted to challenge myself by creating 3D rotating wheels. I felt like the best approach for me was to play with the program itself, without getting any inspiration from other intriguing codes out there. I used p5.js reference list to help me learn new syntaxes. I needed that, considering I only knew how to draw an ellipse. I learned that you can control the weight/thickness of a stroke and fill different lines with different colors. I then decided I wanted spinning wheels even though I hit a wall in the process. Coding can be tough and intimidating when it doesn’t work out the way you wanted it to, but when you succeed you will never forget exactly what you did - I do think that this process will make me learn more. When writing my own code I realized just how precise I needed to be. Reading other people’s code can be intimidating at first, but if you really look through every line and use the reference list for the syntaxes you don’t know, it will eventually make sense.

***Reflection***
Writing my own code, I really had to understand every syntax and be aware of its position. It’s not enough to understand it superficially, because one bug can ruin the entire code. When reading other people’s code you can get an insight to the endless amount of possibilities and syntaxes that exist. When writing code you can express yourself. By doing both you can be inspired by others and vice versa.

The concept of programming is being able to control, design, and express technology. According to Annette Vee’s writing “Coding Literacty” coding is a new part of literacy. The evolution of software is developing so fast and in order to understand this evolution we need to understand coding and how it works - It's a very powerful tool. Knowing and understanding code is essential to get a critical processes excisting in a digitalized society.
