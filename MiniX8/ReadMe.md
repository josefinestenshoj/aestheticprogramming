## MiniX8 - E-LIT

**Try it for yourself [here] (https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX8/)**

**See the code repository (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX8/sketch.js)**


### MiniX8 - Jakob, Maiken, Maria & Josefine

<img src="screen.png" width="700">

MiniX 8 - E-Lit


### Voices of climate 
One of the biggest concerns in the world these days is the climate changes that humans have caused especially since the 1950s. What will happen to humans when the earth cannot handle the higher temperatures, rising water levels, and extreme weather? Climate changes make animals and humans more vulnerable, with rising water levels in low-lying areas citizens might need to find other places to live. Extreme weather plays a huge role in our biodiversity, months of drought can cause species to become extinct because a lot of the animal- and plant life lives under certain conditions. Drought also threatens human beings, especially in developing countries where rainwater is a resource for the farmers and their harvest. 
With our program, we want to draw attention to climate changes, with help from statements written/said by acknowledged people such as actor Leonardo DiCaprio, climate activist Greta Thunberg, and physicist Albert Einstein. With these statements, we hope to make people more aware of the climate changes that are constantly evolving.

### Describe how your program works, and what syntax you have used, and learnt?
Our program consists of a header saying “Oh, so you don’t think climate change is relevant?”, different quotes regarding climate change (that change every four seconds) and the question “does this change your mind?” placed at the bottom. We’ve gathered data/quotes from different famous people (Actors, actist, scientists etc.) and hopefully this will encourage the viewer to think about this issue. We started off by defining our variables and functions. We’ve changed the function’s names from “preload”, “draw”, “setup” etc. to headers such as “save our planet” and “before it’s too late” to draw a connection to the overall theme and create a more expressive and creative looking code - making the code more readable for non-programmers and more poetry–like. We’ve created a JSON file called “YouNeedToUnderstand” where we have listed all of our data which in our case are different quotes. JSON files are great for listing various data, especially when you have a lot of it. We’ve preloaded our JSON file into our sketchfile amongst other files such as sound, images and fonts. In our setup function we have set the framerate to 0,3, to make the different quotes appear slowly enough on the canvas so that the user is capable of reading it. We have added a sound of thunderstorm to play while reading the quotes enhancing the seriousness of climate change. 
We have added a header into our draw function, more specifically in a push(); and pop();, to lock the text from everything else going on in the program. Now, we are running the JSON file - more specifically the data gathered as “Climate change” in random order using the random(); function. We customize the appearance of the text using different textStyle();, textWrap() and make the text appear in the center of the canvas. 


### Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).
The vocable code text describes how a program can be structured poetically and in a way that makes it able to express something without even running the code. We have some few examples in our own code, as we gave some of the variables and assets a name that holds value when put into context. In line 46, we declare the “test” variable and apply the random() syntax. The values of this random() syntax are named in a way that could be thought provoking and raise awareness about the topics we chose to say something about. The functions in our code also have names with meaning. Instead of “setup”, we named it saveOurPlanet, because just like function setup() is the ground on which the code lays, saving our planet is the foundation for a lively future. The context in the code is what gives meaning to the values and names we assign. As Geoff Cox and Alex McLean stated in their Vocable code text:

* “The point is that neither the score nor the program script can be detached from its performance, and in this context this is what makes a program like speech (Vocable code, C. 1, P. 22) *

We could’ve used the commenting tools a bit more, as they are meant to express something in our own language, making it visible for everyone (even those, who don’t understand code). 


### How would you reflect on your work in terms of Vocable Code 
As mentioned above climate changes is one big concern for humanity, but do we actually change our behaviors for the climatem? The politicians are making plans to reduce CO2 expenditure all over the world, but what do you do? 
Four seconds is the time that all the quotes in our program are shown before it is replaced with a new one, because of the different lengths of them it can be hard to have time to read them before they are replaced which is a bit like the climate changes. We have heard and talked about them for years, but it is tricky for normal humans to see them and how they affect the world in other ways than rising water levels and so on. Before we know the climate changes have gone so far that it is too late for us to change our behaviors. 
The lengths of the qoutes become a symbol of when it is too late to change our behaviors. Because the quotes are shown in a random order you never know when it is too late. 

 

## References


