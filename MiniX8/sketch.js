var youNeedToUnderstand;
var climateChanges;
var isReal;
var preload = saveOurPlanet;
var draw = beforeItsTooLate;
var setup = thinkAboutChildren;
//defining our variables and functions



function saveOurPlanet() {
  //preload function
  climateChanges = loadFont("Handwriting.ttf")
  //loading the font for the quotes
  isReal = loadFont("LEMONMILK-Regular.otf")
  //loading the font for the header
  youNeedToUnderstand = loadJSON("youNeedToUnderstand.json");
  //loading the JSON file
  backgroundImg = loadImage("space.gif");
  //loading the space background
  sound = loadSound('storm.mp3');
  //loading sound
}

function thinkAboutChildren() {
  //setup function
  createCanvas(windowWidth, windowHeight);
  frameRate(0.3);
  sound.play();

}

function beforeItsTooLate() { //draw
background(backgroundImg);

push();
  textSize(30);
  textFont(isReal);
  textStyle(BOLD);
  textAlign(CENTER);
  fill(255);
  text("Oh, so you don't think climate change is relevant?", width/2, height/2 - 300);
  text("Does THIS change your mind?", width/2, height/2 + 300);
pop();

let test = random(youNeedToUnderstand.climateChange);

  textFont(climateChanges);
  fill(200);
  textStyle(BOLD);
  textSize(40);
  textWrap(WORD);
  text(test, width/4, height/4, width/2, height/2);
}
