## MiniX10 - Machine Unlearning


### Which sample code have you chosen and why?
We have chosen to look at the ml5.soundClassifier(). Due to the fact that no one in our group has ever worked intensively with sound and/or the usage of a microphone, we looked at this classifier with great interest. It is intriguing to try to understand how data capture works in an audible sense. We wanted to look into the capabilities of machine learning in regards to recognizing voices or sound. 

### Have you changed anything in order to understand the program? If yes, what are they?
We tried to tinker a bit with the “createDiv” syntax, which resembles the “text” syntax in p5.js, as it serves to display a predetermined text. As expected, after changing their names to something different, that something different got displayed in the tinkered code. 

### Which line(s) of code is particularly interesting to your group, and why?
After discussing which themes that could be interesting for the Final project, we figured out that all of us think it is quite exciting to work with data capture. On this basis, we find line 22 classifier = ml5.soundClassifier('SpeechCommands18w', options); interesting. In this line we have the machine learning part of the code, where the words are classified. This can be compared to data capture, because the program needs to capture your voice to classify the words you say.  


### How would you explore/exploit the limitation of the sample code/machine learning algorithms?
Each member of the group tried to speak the following commands to the microphone: “ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'up', 'down', 'left', 'right', 'go', 'stop', 'yes', and 'no'”. We experienced that the microphone did not always catch the exact word we said and if it did the “confidence” level would be around 0.60-0.70.  You had to be articulating the word very precisely to reach a high confidence level. Thinking of the audible part of machine learning, it can clearly exclude people if you are not able to articulate words to the right pronunciation. It could for instance be if you have an accent, speech disorder etc. You also have to be pretty close to the microphone for it to recognize the word you are saying. This could be problematic if you use this function on your phone to “write” a text through the microphone. If it does not recognize the right words you can end up writing something totally different than what intended. 

### What are the syntaxes/functions that you don't know before? What are they and what have your learnt?
confidence = createDiv('Confidence: ...'); This syntax also has an additional feature that makes it able to “store” or hold certain values that can be summoned/called later. In this case these values are the “results” that are called upon (as an array) the label and confidence variables.

classifier.classify(gotResult); This in an explanation from:https://openprocessing.org/sketch/800230/ Expects a callback function as its 2nd argument which will be called with the classification results once they're ready. ml5.js uses an "error-first" callback pattern, which means that our gotResult() callback needs to take an error parameter as its first argument. If err is null, then there were no errors and the classification was successful. The 2nd argument passed to our gotResult() callback is an array of results ordered by confidence. Here, we're only interested in the first, most likely result.

### How do you see a border relation between the code that you are tinker with and the machine learning applications in the world (e.g creative AI/ voice assistances/driving cars, bot assistants, facial/object recognition, etc)?
We can draw a clear connection bewteen the mI5.js and the machine learning applications that are featured in various artifacts such as the voicecommand in Teslacars, Siri on our Iphones, Alexa etc. The same principles applies: when words are being said, it’s a way of communicating with the artifacts - you’re able to make various commands which the program is able to recognize, label and categorize. 

### What do you want to know more/ What questions can you ask further?
It could be interesting to expand our horizon in regards to the technical and conceptual capabilities of the ml5js libraries. They come with a lot of features we haven’t seen before and could prove quite useful in the future. 

