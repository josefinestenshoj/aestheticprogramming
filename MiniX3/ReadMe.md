## MiniX3 submission


### MiniX3 THROBBERS

<img src="MiniX3.png" width="600">

**Try it for yourself [here] (https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX3/)**

**See the code repository (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX3/sketch.js)**


**BASIC DESCRIPTION OF MY MINIX3**

**What do you want to explore and/or express?**
For my third MiniX I wanted to explore new syntaxes (such as loops and/or arrays) and still keep it pretty simple, making sure I understood every single line of my code. I made tiny ellipses moving around and a text saying “Wait..”. I want to express how we deal with time and temporality - especially towards computers. 

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**
I have used “for loop” (for (let i = 0; i < 1000; i++)) in my program. I started off by defining my “counter” which is “I” starting at 0. Then I set the condition - letting i be less than 1000 ellipses and lastly my incrementalss I++. This makes a thousand ellipses appear on the screen. I wanted to make them move in some kind of way that makes it seem kind of stressful. I used the “move” function and make them move randomly within a small area of coordinates (-1,1). This makes the circle run kind of stressfully. With the text WAIT I wanted to address the lack of patience most of us feel when we meet a throbber or loading screen.  I was inspired by Daniel Schiffman’s video “Arrays of objects” (https://www.youtube.com/watch?v=fBqaA7zRO58) for this project. 

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

For must of us, our association with a throbber is an irritating or even frustrating feeling. Throbbers are often very “satisfying” and makes us follow the line it moves around. I wanted to make the exact opposite and emphasize the frustration and stress level you feel when something is not working. The small circles are meant to represent small braincells working on full speed. I often feel like, we humans want our technology to work super smoothly all the time (like AI, with no flaws), but we don’t have the same expectations towards our fellow human beings. It is probably, because most of us doesn’t understand what is going on “behind the screen” on our computer and therefore are less willing to tolerate its flaws. I found this quote from Hans Lammerant’s chapter “How humans and machines negotiate experience of time,” in his book The Techno-Galactic Guide to Software Observation, quite interesting, because it adresses how technology has changed our natural cycle of time: “In contrast to the earthly or natural cycles it is with- out any seasonal difference or interference between several cy- cles. This makes a computer into a monadic time capsule, dis- connected from the outside rhythms. The time experience in the software is in principle based on this local time cycle.” (p. 94)

It’s super interesting to look at how different softwares uses different throbbers to “distract” people from the waiting period it rises. I specifically remember some of the old Window’s computers had a screensaver showing pipes going different directions and I remember these had an effect (for me personally) that time went by faster than usual. 

Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018), https://monoskop.org/log/?p=20190.
