## ALGORITHMIC PROCEDURES 
### MINIX9 - Flowcharts
​​

### INDIVIDUAL WORK
I chose to make a flowchart of my miniX6 (Object oriented programming), because i find this code to be the most challenging and complex one I have ever made. It was super helpful to make a flowchart afterwards and I will most definently incoperate this in my future miniX. It is a great way to create an overview of the work and makes it easier to begin programming.

![](Flowchart.png)

### GROUP WORK 
For our ninth MiniX we had to brainstorm two ideas for our final project and then draw two flowcharts to visualize the project’s algorithmic processes. We were already very intreged by climate change and data capture and knew that we wanted to work on that one way or another - We ended up with these two ideas:

### FIRST IDEA
![](Idea1.png)


### SECOND IDEA
![](Idea2.png)


#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
Keeping it simple in regards to flowcharts is not only essential for the process of comprehension (for non-programmers), but also to increase the understanding of the concept at hand for the developers themselves. In order to explain the technical and conceptual process to someone that is not familiar with coding, a well formulated flowchart goes a long way. It shows the process in small steps and provides a structured description of the machine's logic. The difficulty involved in producing such a flowchart lies in making the explanation accessible for “everyone”. How exactly do you explain a for loop or a conditional statement in non-technical terms? But trying to explain and formulate a “simple” flowchart gives a lot of ground for discussion for the developers themselves. By discussing the relevant topics and iterating the concept, a greater understanding of the project is likely to be achieved.

#### What are the technical challenges facing the two ideas and how are you going to address these?

In our case, having two ideas actually proved to be quite useful. It expanded our thoughts in different directions and we ended up having a lot of good points for both of our ideas. After some discussing and idea-tweaking we actually ended up melting the ideas together and creating a single, well-rounded concept for the final project. The flowchart we created proposes a lot of syntaxes or conditional structures that we predict to be manageable, without being quite sure of it. The difficulty or problem here is that we basically have to predict what we are able to do and what not. We have to be ambitious to create a good project, but at the same time we also need to be realistic in regards to our coding skills in order to make our ambitions and goals of this project come true.



#### In which ways are the individual and the group flowcharts you produced useful?
The group flowcharts have been useful for collecting collective ideas and making sure every member of the group is on the same page regarding the technicalities of the code as well as the conceptual part. It is a great way to walk through the algorithmic procedures of the program (syntaxes, functions and so on) while everyone is on board. It will also be easier later on with this flowchart, when wanting to change and develop the program together in a collaborative mind by breaking down the program into smaller pieces. We now have an overall idea of what we want to code and a flowchart to guide us. This can also be quite suitable for group work because you can split the different parts of the program and work on them individually, while still making sure everyone knows what is going on. 
Working with flowcharts both individually and together challenges you as a coder to make sure you know exactly what the code does and why we do it. Working individually with one of our own code repositories forces us to rethink our program and make sure we understand every single part of our code. It is also a great exercise for us to think backwards and understanding why we made certain decisions to think about later on in future coding experiences. 


