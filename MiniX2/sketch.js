function setup() {
  //put setup code here
  createCanvas(windowWidth,windowHeight);
  //Create a canvas/foundation that is windowWidth x windowHeight pixels
  frameRate(10);
  //set 10 frames per second
  background(232,227,213);


}
function draw() {
  //Draw function to start my drawing
  const w="HOW DO YOU ACT ON SOCIAL MEDIA?"
  //creating a constant w
  fill(39,39,91);
  noStroke();
  textSize(40);
  text(w,200,100);
  //Changing the texts colour, size and location


//KIND BUTTON
  const nice=createButton("Press me to be kind");
  nice.size(150,50);
  nice.position(100,200);
  nice.mousePressed(heart);
  /*creating constant and button 'nice' with text "Press me to be kind"
  Changing size and position with coordinates
  Using function mousePressed(heart) to make
  heart appear when the button is pressed*/

//MEAN BUTTON
  const mean=createButton("Press me to be mean");
  mean.size(150,50);
  mean.position(100, 300);
  mean.mousePressed(brokenHeart);
  /*creating constant and button 'mean' with text "Press me to be mean"
  Changing size and position with coordinates
  Using function mousePressed(brokenHeart) to make
  brokenHeart appear when the button is pressed*/


//START OVER BUTTON
  const clear=createButton("Start over");
  clear.size(100,30);
  clear.position(50,750);
  clear.mousePressed(clearCanvas);
  /*creating constant and button 'cleat' with text "Start over"
  Changing size and position with coordinates
  Using function mousePressed(clear) to make
  the canvas clear when the button is pressed*/
}

//HEART
function heart() {
  for (let i = 0; i < 10; i++) {
    /*'for' creates a loop that is useful for executing
    one section of code multiple times
    let i = 0 (setting the inital state for the loop)
    i < 10 (condition checked before the loop starts: i has to be less than 10)
    i++ (executed at the end of each loop: an increment returning to the value)*/
    let offsetY = random(0, windowHeight);
    let offsetX = random(0, windowWidth);
    /*Declaring offsetY and offsetX variables to 'lock' its value
    and be able to make the hearts appear randomly on the canvas*/



//Drawing the heart
    fill(97,48,45)
    strokeWeight(1);
    stroke(121,71,68);
    beginShape();
    curveVertex(offsetX + 400, offsetY + 600);
    curveVertex(offsetX + 150, offsetY + 300);
    curveVertex(offsetX + 35, offsetY + 150);
    curveVertex(offsetX + 80, offsetY + 80);
    curveVertex(offsetX + 150, offsetY + 130);
    curveVertex(offsetX + 150, offsetY + 300);
    endShape();
    // All of these numbers are constants, but the offsetX and offsetY are variables randomly generated

    beginShape();
    curveVertex(offsetX + 150,offsetY + 300);
    curveVertex(offsetX + 150,offsetY + 130);
    curveVertex(offsetX +220, offsetY + 80);
    curveVertex(offsetX + 270, offsetY + 140);
    curveVertex(offsetX + 150, offsetY + 300);
    curveVertex(offsetX + 100, offsetY + 400);
    endShape();
  }
  }

//BROKEN HEART
function brokenHeart() {
  for (let i = 0; i < 10; i++) {
  /*'for' creates a loop that is useful for executing
  one section of code multiple times
  let i = 0 (setting the inital state for the loop)
  i < 10 (condition checked before the loop starts: i has to be less than 10)
  i++ (executed at the end of each loop: an increment returning to the value)
  a loop that runs 10 times*/


  let offsetY = random(0, windowHeight);
  let offsetX = random(0, windowWidth);
  /*Declaring offsetY and offsetX variables to 'lock' its value
  and be able to make the hearts appear randomly on the canvas*/


//Drawing the heart
    fill(31,43,68);
      strokeWeight(1);
      stroke(88,99,125);
      beginShape();
      curveVertex(offsetX + 360, offsetY + 460);
      curveVertex(offsetX + 360, offsetY + 460);
      curveVertex(offsetX + 268, offsetY + 375);
      curveVertex(offsetX + 180, offsetY + 261);
      curveVertex(offsetX + 190, offsetY + 160);
      curveVertex(offsetX + 255, offsetY + 120);
      curveVertex(offsetX + 320, offsetY + 158);
      curveVertex(offsetX + 360, offsetY + 210);
      curveVertex(offsetX + 320, offsetY + 260);
      curveVertex(offsetX + 360, offsetY + 310);
      curveVertex(offsetX + 320, offsetY + 360);
      curveVertex(offsetX + 360, offsetY + 410);
      curveVertex(offsetX + 360, offsetY + 410);
      endShape();
      //other half of the heart

      beginShape();
      curveVertex(offsetX + 370, offsetY + 460);
      curveVertex(offsetX + 370, offsetY + 460);
      curveVertex(offsetX + 452, offsetY + 375);
      curveVertex(offsetX + 540, offsetY + 261);
      curveVertex(offsetX + 530, offsetY + 160);
      curveVertex(offsetX + 465, offsetY + 120);
      curveVertex(offsetX + 400, offsetY + 158);
      curveVertex(offsetX + 370, offsetY + 220);
      curveVertex(offsetX + 330, offsetY + 260);
      curveVertex(offsetX + 370, offsetY + 310);
      curveVertex(offsetX + 330, offsetY + 360);
      curveVertex(offsetX + 370, offsetY + 410);
      curveVertex(offsetX + 370, offsetY + 410);
      endShape();
      //other half of the heart
}
}

//START OVER BUTTON
function clearCanvas() {
  background(232,227,213);
  //Making the background so that it clears the canvas

}
