## MiniX2 submission


### MiniX2 EMOJIS

<img src="MiniX2.png" width="600">

**Try it for yourself [here] (https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX2/)**

**See the code repository (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX2/sketch.js)**


**BASIC DESCRIPTION OF MY MINIX2**

My program consist of two buttons saying: "press me to be kind" and "press me to be mean" and a header asking: "How do you act on social media?". When you click on the "kind" button ten red heart will appear randomly on the screen. If you click the "mean" button ten broken hearts will appear randomly. Its then up to you if you want to fill the screen with love (red hearts ❤️) or cruelness (broken hearts 💔). Lastly you have the option to clear the canvas by clicking the "start over" button. I've really challenged myself with this MiniX2, using many new syntaxes while still trying to understand every line.
I have obviously worked with different buttons (createButton) and (mousePressed) syntaxes. Besides that I've worked with different variables to make the heart appear randomly on the screen following every coordinate of the hearts i created in curveVertex.

**WHY DID I CHOOSE TO DO THESE SPECIFIC EMOJIES?**

When thinking of emojis I couldn't help thinking of communication on social media in general. We use emojis to express ourselves and we often interpret words and emojis differently and it can easily be misunderstood by the receiver. It's also very easy to write something online that you never would have said face to face, because we can't see how its received and how it makes the other person(s) feel. I also took a look at what emojis I used the most. I used the heart emoji the most and I thought i remake a heart.


**REFLECTION OF MY PROGRAMMING EXPERIENCE SINCE LAST MINIX AND EMOJI IN A WIDER SOCIAL CONTEXT**

I've learned a lot since my last excersise - Creating vairables, getting more comfortable drawing and having some kind of interaction in my program. 

My intention with my MiniX2 was to make the user reflect upon their own use of social media and how they communicate with others. By being kind you spread love (hearts) and by being mean you break hearts and the hearts are bigger, to show how much a mean comment can effect others. I think the text "modifying the universal" was super interesting - looking at how we actually exlude a minority of people when creating emojis. Often it is not something I think is done on purpose, but it is so important to be aware of. This is the reason why I did not create a new emoji, i felt like did not exist or has been excluded. Sometimes when you want to include someone, you tend to exlude others in the proces. My emoji focuses more on the overall problem of excluding or not acting kind in a social context. It is meant to be something that everyone can relate to, regardless of age, gender, sexuality etc. But it hard to know if you have excluded someone, and frankly I don't know if I have myself. Therefore I think it's always important to include different people in the proces to make sure you make your (in this case emoji) as accessable as possible.


##REFERENCE

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016 [Video, 1 hr 15 mins].

Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. [watch 1.3,1.4,2.1,2.2]

