//Declaring global variables
let canvas;
let textbox;
let describe;
let firstheader;
let secondheader;
let fontheader;
let greeting;

//Buttons
let submitButton
let yesButton;
let noButton;
let calculatedButton;
let startoverButton;

//IMAGES
let warning;
let warning2;

//CALCULATIONS
let digitalGreenprintCounter = 0;
let digitalGreenprintCounterText;
let digitalGreenprintCalc = 0.08;
let digitalGreenprintCalcText = 0.08;

//RANKING LIST
let best;
let test;

//Preloading fonts, JSON and images
function preload(){
  fontheader = loadFont('BebasKai.ttf');
  namefont = loadFont('Basic Light.ttf');
  numbers = loadJSON("numbers.json");
  highRank= loadJSON("highestScore.json")
  middleRank = loadJSON("middleScore.json")
  lowRank = loadJSON("lowestScore.json")
  whyamiseeingthis = loadImage("datacapture.png");
  warningimg = loadImage("Warning.png");
  warning2img = loadImage("Warning2.png")
}

function setup() {

  canvas = createCanvas(windowWidth,windowHeight);
  noStroke();
  frameRate(40);


  setupsketch();
}

function draw() {
  digitalGreenprintCounter+= 1/60
  if(digitalGreenprintCounterText) {
    digitalGreenprintCounterText.html(digitalGreenprintCounter);

  digitalGreenprintCalc = canvas.mousePressed();

  }
}
function setupsketch() {

  background(247,242,242);
  //making the headline
    textAlign(CENTER);
    textSize(45);
    textFont(fontheader);
    firstheader = text('Type your full name to see your digital greenprint',windowWidth/2,windowHeight/2);

    //making the thing you type in
    textbox = createInput();
    textbox.position(windowWidth/2-96,windowHeight/2+40);

  //making a bottom, where you can submit your name
  submitButton = createButton('SUBMIT');
  submitButton.position(windowWidth/2-54,windowHeight/2+80);
  submitButton.size(100,40);
  submitButton.addClass('Button1');
  submitButton.addClass('Button:hover');
  submitButton.mousePressed(popup1);

  //creating the image taken when clicking 'yes'
  camcorder = createCapture(VIDEO);
  camcorder.size(800,800);
  camcorder.hide();

}

function reset() {
  setupsketch();
  startoverButton.remove();

}

//Pop up window aflter clicking 'submit'
function popup1(){

  //inserting popup image with warning
  warning = image(warningimg,windowWidth/2-350,windowHeight/2-300,750,600);

  //creating the yes button which enables interface3
  yesButton = createButton('YES')
  yesButton.position(windowWidth/2+100,windowHeight/2+50);
  yesButton.size(70,40);

  //removing the textbox and submitbutton so it doesn't interfere with the warning image
  textbox.remove();
  submitButton.remove();

  //By clicking YES button interface3 will appear
  yesButton.mousePressed(interface3);

  //By clicking the NO button interface 2 will appear
  noButton = createButton('NO')
  noButton.position(windowWidth/2+180,windowHeight/2+50);
  noButton.size(70,40);
  noButton.mousePressed(interface2);

}

function interface2() { //Second warning with the opportunity to start over

  warning2 = image(warning2img,windowWidth/2-400,windowHeight/2-350,870,700);

  //Creating the start over button
  startoverButton = createButton('START OVER')
  startoverButton.position(windowWidth/2+180,windowHeight/2+50);
  startoverButton.size(70,40);

  //removing the buttons from previous interface
  yesButton.remove();
  noButton.remove();

  //by clicking the start over button you will return to the first interface
  startoverButton.mousePressed(reset);

}

function interface3() { //digital greenprint interface

  //styling the website
  background(247,242,242);
  stroke(216,216,216);
  strokeWeight(3);
  line(0, 60, width, 60);
  line(width/2-10, 60, width/2-10, height);

  //removing buttons from previous interfaces
  submitButton.remove();
  yesButton.remove();
  noButton.remove();
  textbox.remove();

  //loading the image which is captured when the user clicks "yes"
  image(camcorder,windowWidth/2-550,windowHeight/2-200,345,260);

  //Header "My Digital Greenprint"
  stroke(10);
  strokeWeight(1);
  textAlign(CENTER);
  textSize(45);
  fill(32,63,16);
  text('MY DIGITAL GREENPRINT',windowWidth/2,windowHeight/2-350);

  //Ellipse with greenprint percentage inside
  ellipse(windowWidth/2+280,windowHeight/2-110,300,300);
  fill(32,63,16);

  //Generating a random number within the array "Greenprint" in the JSON file
  let test = random(numbers.Greenprint)
  fill(255);
  textSize(150);
  text(test,windowWidth/2+150,windowHeight/2-200,300,300);

  //The user's name typed from the beginning is shown on the left side
  let name = textbox.value();
  fill(32,63,16);
  textSize(40);
  textFont(namefont);
  text(name,windowWidth/2-450,windowHeight/2-250);

  // Counter of CO2 emission using this website
  digitalGreenprintCounterText = createDiv(digitalGreenprintCounter);
  digitalGreenprintCounterText.style('font-size','40px');
  digitalGreenprintCounterText.style('text-align','center');
  digitalGreenprintCounterText.style('font','fontheader');
  digitalGreenprintCounterText.position(windowWidth/2-500,windowHeight/2+100);
  digitalGreenprintCounterText.style('z-index','0');

  //Creating and styling button with calculations
  push();
  calculatedButton = createButton('HOW IS THIS CALCULATED?');
  calculatedButton.position(windowWidth/2+180,windowHeight/2+200);
  calculatedButton.size(200,100);
  calculatedButton.addClass('Button1');
  calculatedButton.addClass('Button:hover');
  calculatedButton.mousePressed(interface4);
  pop();
}

  function interface4() {
    calculatedButton.remove();
    digitalGreenprintCounterText.hide();
    //image(whyamiseeingthis,windowWidth/2-500,windowHeight/2-450,1100,800);


    /* let x = 0;
    let y = 0;
    while (x < n.length) {
      text(n[x],50,(y+i) % windowHeight);
      x++;
      y+=50; */

if (test == numbers.Greenprint[0] || numbers.Greenprint[1] || numbers.Greenprint[2] || numbers.Greenprint[3] || numbers.Greenprint[4] || numbers.Greenprint[5]){
  text(lowRank.Best, windowWidth/2, windowHeight/2);
  }

let x = 0
let y = 0
while (x < lowRank.length) {
  text(lowRank[x].Best, 50, y)
  x++;
  y+=50;
}



if (test == numbers.Greenprint[7] || numbers.Greenprint[8] || numbers.Greenprint[9] || numbers.Greenprint[10] || numbers.Greenprint[11] || numbers.Greenprint[12] || numbers.Greenprint[13]){
  text(middleRank.Middle,windowWidth/2, windowHeight/2);

 }
if (test == numbers.Greenprint[14] || numbers.Greenprint[15] || numbers.Greenprint[16] || numbers.Greenprint[17] || numbers.Greenprint[18] || numbers.Greenprint[19] || numbers.Greenprint[20]){
  text(highRank.worst,windoeWidth/2, windowHeight/2);

}


/*
ON THE LEFT SIDE:
Insert JSON file withing the 1100x800 image with scrolling text
depending on which score you get
if you get under 33% 3.json will appear
if you get between 33% and 67% 2.json will appear
if you get over 67% 1.json file will appear

ON THE RIGHT SIDE:
What you have used just by running this program
takes the value from the framecount and how many times you've clicked the screen


probably use if else statement from p5.js
*/

}
