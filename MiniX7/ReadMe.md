## MiniX7 - REVISIT THE PAST

**Try it for yourself [here] (https://josefinestenshoj.gitlab.io/aestheticprogramming/MiniX7/)**

**See the code repository (https://gitlab.com/josefinestenshoj/aestheticprogramming/-/blob/main/MiniX7/sketch.js)**


### MiniX7 

<img src="screen.png" width="700">


**WHICH MINIX HAVE YOU REWORKED?**
I chose to rework my miniX3 - back when we worked with temporality - specifically throbbers. When creating that previous miniX I worked with some syntaxes and coding related to object oriented programming which I only understood briefly at the time. I remember being very intrigued by Shiffman’s video and the structure the code suddenly had by introducing personal functions and classes. I thought this was a great chance to revisit this miniX, now that I’ve worked with Object oriented programming in the previous miniX related to games - to gain confidence and to change some things with the exciting code to make it more fun. 


**WHAT HAVE YOU CHANGED AND WHY?**
First of all I really wanted to make a separate class for the circles in my program, because I didn’t make that the first time. I created a class in a separate javascript file (circle.js) and formed my object by constructing it (x,y,r) and adding it into my sketch (new Circle). 

After discussing temporality in time and our relationship with time in a digital culture I had new ideas for my throbber. I wanted the circles (which in my vision represents brain cells) to disappear when the user interacts with them. I think distraction is something that can help time pass by even faster for most of us. I also wanted to mix my throbber with the game aspect of last weeks theme into my renewed throbber. I did this by creating a function “clicking” in my circle.js class. To make sure that the circle only disappear when clicking on it I defined let d = dist(px, py, this.x, this.y); - as a way to make it as accurate as possible. I now create a boolean - if d Is bigger than the radius of the circle (true) it will splice within the circles array, making that specific circle disappear. 

Lastly, I also changed the text and the movement of each circle. I wrote “help me load” instead of “wait”, to invite the user to interact with the throbber, and make the user feel some sort of “invited” and be able to do something. I also made the movement of each circle a bit bigger to emphasize the stress of time/throbber. 


**What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?**

It was interesting for me to see how I looked at some of my old code very differently from earlier. I also feel like the conceptual parts of every theme each week slowly start to intertwine. Exactly how I interpreted my renewed miniX mixing the concept of time and temporality with games and object oriented programming. It’s super interesting to me, how great of an impact a thing such as a throbber or a game can have an impact on the user in a psychological way. We can make something such an abstract thing as time and change the user’s relationship with it just by changing a few parameters (in this case, making the user interact with the throbber - maybe influencing them to think the waiting period were shorter than what it in reality was). 



**What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?**
Aestetic programming shows a much more dynamic and reflected view on the digital culture and on programming itself. It digs in to important issues within the digital culture and merging it together with the more technical part of programming and programming as literacy. Just as mentioned in the preface of the book “Aesthetic Programming”: 

“The argument the book follows is that computational culture is not just a trendy topic to study to improve problem-solving and analytical skills, or a way to understand more about what is happening with computational processes, but is a means to engage with programming to question existing technological paradigms and further create changes in the technical system. We therefore consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities, in order to act upon those realities. “ (p. 14)


 Earlier on programming and software engineering in general were ruled primarily by white men, and the field really lacked accesibility and diversity. The p5.js community makes coding accesable for everyone regarding race, sexuality, level of skill and etc. With the fast development of the digital society, we must not forget the deep societal and political issues that is very fused into social media and digital objects in general. I think it’s crucial for us to be aware and critical towards digital culture - making (for instance) coding more accesable is one step for people to gain awareness and learn what issues and/or benefits this culture raises. Since it is such a (relatively) new field, we still have the ability to influence and shape the future of the digital culture - maybe making it more accessible and overall a healthy environment.


## References


[7.3: Arrays of Objects - p5.js Tutorial](https://www.youtube.com/watch?v=fBqaA7zRO58)

[Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020]



