//Naming the variable and array
let circles = [];


function setup() {
 createCanvas(windowWidth, windowHeight);
  //create a canvas/foundation
 frameRate(200);
  //setting the framework, so the bubbles moves smoothly
 for (let i = 0; i < 1000; i++) {
  let x = random(width);
  let y = random(height);
  let r = random(10,30)
  let b = new Circle(x,y,r);
   circles.push(b);
   //push adds something to the end of the array 
  }
}
 //create a loop starting at 0, execute it
 //check to see if it's less than a thousand, go up by 1, checking again


function mousePressed() {
 for (let i = 0; i < circles.length; i++){
   if (circles[i].clicked(mouseX,mouseY)) {
   circles.splice(i,1);
   // if a circle i clicked it will be removed
   //it removes the element at index i in the circles array 
   }
 }
}

  function draw() {
      background(6,6,6);
      noStroke();
      textSize(150);
      textAlign(CENTER);
      fill(200);
      text('help me load...',700,700);


      for (let i = 0; i < circles.length; i++){
          //Adding to the array, refering to previous loop
        circles[i].move();
        circles[i].show();

      }
      }

      function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
