class Circle {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }

  clicked(px, py) {
    let d = dist(px, py, this.x, this.y);
    //px and py are my input variables and this.x and this.y are my center of the circle
    return d < this.r;
    //d < this.r evaluates to true or false, so i can just return the value of this expression
  }

  move() {
    this.x = this.x + random(-2, 2);
    this.y = this.y + random(-2, 2);
  }

  show() {
    strokeWeight(1);
    stroke(88,119,172);
    fill(68,99,152)
    ellipse(this.x, this.y, this.r);
  }
}
